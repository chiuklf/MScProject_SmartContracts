var SetVariables = artifacts.require("./SetVariables.sol");
var SetVariables_modified = artifacts.require("./SetVariables_modified.sol");
var SetVariables_Ordering = artifacts.require("./SetVariables_Ordering.sol");
var ReEntrancy = artifacts.require("./ReEntrancy.sol");
var ReEntrancy_modified = artifacts.require("./ReEntrancy_modified.sol");
var Arithmetic = artifacts.require("./Arithmetic.sol");
var Arithmetic_modified = artifacts.require("./Arithmetic_modified.sol");
var Uncheck = artifacts.require("./Uncheck.sol");
var Uncheck_modified = artifacts.require("./Uncheck_modified.sol");


module.exports = function(deployer) {
  deployer.deploy(SetVariables);
  deployer.deploy(SetVariables_modified);
  deployer.deploy(SetVariables_Ordering);
  deployer.deploy(ReEntrancy);
  deployer.deploy(ReEntrancy_modified);
  deployer.deploy(Arithmetic);
  deployer.deploy(Arithmetic_modified);
  deployer.deploy(Uncheck);
  deployer.deploy(Uncheck_modified);
};
