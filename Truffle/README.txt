To run the tests, you would have to have to run Ganache with host: "127.0.0.1" and port: 7545. Ganache is provided in this folder or you can download Ganache from https://github.com/trufflesuite/ganache/releases.

Then run on terminal:

truffle compile

truffle migrate --reset

truffle test


To execute the script:

truffle exec gasConsumption.js