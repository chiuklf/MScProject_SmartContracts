const Arithmetic = artifacts.require('./Arithmetic.sol')
contract('Arithmetic', function ([owner, donor]) {

    let catchRevert = require("./exception.js").catchRevert;
    let BigNumber = require("bignumber.js");
    let arithmetics;

    beforeEach('setup contract for each test', async function () {
        arithmetics = await Arithmetic.new()
    });

    it('noBrackets Test', async function() {
      await arithmetics.noBrackets()
      let a = await arithmetics.a();
      assert.equal(a, 14);
    });

    it('withBrackets Test', async function() {
      await arithmetics.withBrackets()
      let a = await arithmetics.a();
      assert.equal(a, 35);
    });

    it('inConditions Test', async function() {
      await arithmetics.inConditions()
      let a = await arithmetics.a();
      assert.equal(a, 99);
    });

    //Big number needed to handle 256-bit number
    it('Underflow Test', async function() {
      await arithmetics.underflow()
      let underflowValue = new BigNumber("115792089237316195423570985008687907853269984665640564039457584007913129639923");
      let a = await arithmetics.a();
      let result = new BigNumber(a);
      assert.equal(result.minus(underflowValue), 0);
    });

    it('Overflow Test', async function() {
      await arithmetics.overflow()
      let a = await arithmetics.a();
      assert.equal(a, 0);
    });

})
