const Arithmetic_modified = artifacts.require('./Arithmetic_modified.sol')
contract('Arithmetic_modified', function ([owner, donor]) {

    let catchRevert = require("./exception.js").catchRevert;
    let BigNumber = require("bignumber.js");
    let arithmetics;

    beforeEach('setup contract for each test', async function () {
        arithmetics = await Arithmetic_modified.new()
    });

    it('noBrackets Test', async function() {
      await arithmetics.noBrackets()
      let a = await arithmetics.a();
      assert.equal(a, 14);
    });

    it('withBrackets Test', async function() {
      await arithmetics.withBrackets()
      let a = await arithmetics.a();
      assert.equal(a, 35);
    });

    it('inConditions Test', async function() {
      await arithmetics.inConditions()
      let a = await arithmetics.a();
      assert.equal(a, 99);
    });

    //SafeMath will revert if underflow or overflow
    it('Underflow Revert Test', async function() {
      await catchRevert(arithmetics.underflow());
      let a = await arithmetics.a();
      assert.equal(a, 1);
    });
	
    it('Overflow Revert Test', async function() {
      await catchRevert(arithmetics.overflow());
      let a = await arithmetics.a();
      assert.equal(a, 1);
    });

})
