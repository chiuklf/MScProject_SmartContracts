pragma solidity ^0.4.24;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/SetVariables.sol";
import "../contracts/SetVariables_modified.sol";

contract TestDelegateCallDynamic {

  function testDelegateSetBytes() public {
    SetVariables setContract = SetVariables(DeployedAddresses.SetVariables());
    SetVariables_modified delegateContract = SetVariables_modified(DeployedAddresses.SetVariables_modified());
    bytes memory expected = "ABC";
    delegateContract.delegatecallsetBytes(DeployedAddresses.SetVariables(), expected);
    bytes memory result = delegateContract.bytesN();
    bytes memory origin = setContract.bytesN();
    bytes memory expected_origin = "XYZ";
    Assert.equal(result[0], expected[0], "SetVariables_modified on bytes");
    Assert.equal(result[1], expected[1], "SetVariables_modified on bytes");
    Assert.equal(result[2], expected[2], "SetVariables_modified on bytes");
    Assert.equal(origin[0], expected_origin[0], "SetVariables_modified should not change callee contract");
    Assert.equal(origin[1], expected_origin[1], "SetVariables_modified should not change callee contract");
    Assert.equal(origin[2], expected_origin[2], "SetVariables_modified should not change callee contract");
  }

  function testDelegateSetString() public {
    SetVariables setContract = SetVariables(DeployedAddresses.SetVariables());
    SetVariables_modified delegateContract = SetVariables_modified(DeployedAddresses.SetVariables_modified());
    string memory expected = "ABC";
    delegateContract.delegatecallsetString(DeployedAddresses.SetVariables(), expected);
    string memory result = delegateContract.stringN();
    string memory origin = setContract.stringN();
    Assert.equal(result, expected, "SetVariables_modified on String"); 
    Assert.equal(origin, "Hello", "SetVariables_modified should not change callee contract"); 
  } 

  function testDelegateDynamicArray() public {
    SetVariables setContract = SetVariables(DeployedAddresses.SetVariables());
    SetVariables_modified delegateContract = SetVariables_modified(DeployedAddresses.SetVariables_modified());
    uint[] memory expected = new uint[](3);
    expected[0] = 111;
    expected[1] = 222;
    expected[2] = 333;
    delegateContract.delegatecallsetDynArray(DeployedAddresses.SetVariables(), expected);
    Assert.equal(delegateContract.dynArrN(0), expected[0], "SetVariables_modified on dynamic array");
    Assert.equal(delegateContract.dynArrN(1), expected[1], "SetVariables_modified on dynamic array");
    Assert.equal(delegateContract.dynArrN(2), expected[2], "SetVariables_modified on dynamic array");
    Assert.equal(setContract.dynArrN(0), 0, "SetVariables_modified should not change callee contract");
    Assert.equal(setContract.dynArrN(1), 0, "SetVariables_modified should not change callee contract");
    Assert.equal(setContract.dynArrN(2), 0, "SetVariables_modified should not change callee contract");
  }

}
