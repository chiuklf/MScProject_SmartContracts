pragma solidity ^0.4.24;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/SetVariables.sol";
import "../contracts/SetVariables_modified.sol";

contract TestDelegateCallDynamicTwoArgs {

  function testDelegateStringAndBytes() public {
    SetVariables setContract = SetVariables(DeployedAddresses.SetVariables());
    SetVariables_modified delegateContract = SetVariables_modified(DeployedAddresses.SetVariables_modified());
    string memory expectedString = "Expected";
    bytes memory expectedBytes = "ABC";

    delegateContract.delegatecallsetStringAndBytes(DeployedAddresses.SetVariables(), expectedString, expectedBytes);

    string memory resultString = delegateContract.stringN();
    bytes memory resultBytes = delegateContract.bytesN();

    string memory originString = setContract.stringN();
    bytes memory originBytes = setContract.bytesN();
    bytes memory expected_originBytes = "XYZ";

    Assert.equal(resultString, expectedString, "Delegatecall on string as first argument");
    Assert.equal(resultBytes[0], expectedBytes[0], "Delegatecall on bytes as second argument");
    Assert.equal(resultBytes[1], expectedBytes[1], "Delegatecall on bytes as second argument");
    Assert.equal(resultBytes[2], expectedBytes[2], "Delegatecall on bytes as second argument");

    Assert.equal(originString, "Hello", "Delegatecall should not change callee contract"); 
    Assert.equal(originBytes[0], expected_originBytes[0], "Delegatecall should not change callee contract");
    Assert.equal(originBytes[1], expected_originBytes[1], "Delegatecall should not change callee contract");
    Assert.equal(originBytes[2], expected_originBytes[2], "Delegatecall should not change callee contract");
  }

  function testDelegateBytesAndArray() public {
    SetVariables setContract = SetVariables(DeployedAddresses.SetVariables());
    SetVariables_modified delegateContract = SetVariables_modified(DeployedAddresses.SetVariables_modified());
    bytes memory expectedBytes = "ABC";
    uint[] memory expectedArray = new uint[](3);
    expectedArray[0] = 111;
    expectedArray[1] = 222;
    expectedArray[2] = 333;

    delegateContract.delegatecallsetBytesAndArray(DeployedAddresses.SetVariables(), expectedBytes, expectedArray);
    
    bytes memory resultBytes = delegateContract.bytesN();

    bytes memory originBytes = setContract.bytesN();
    bytes memory expected_originBytes = "XYZ";
    
    Assert.equal(resultBytes[0], expectedBytes[0], "Delegatecall on bytes as second argument");
    Assert.equal(resultBytes[1], expectedBytes[1], "Delegatecall on bytes as second argument");
    Assert.equal(resultBytes[2], expectedBytes[2], "Delegatecall on bytes as second argument");
    Assert.equal(delegateContract.dynArrN(0), expectedArray[0], "Delegatecall on dynamic array as second argument");
    Assert.equal(delegateContract.dynArrN(1), expectedArray[1], "Delegatecall on dynamic array as second argument");
    Assert.equal(delegateContract.dynArrN(2), expectedArray[2], "Delegatecall on dynamic array as second argument");

    Assert.equal(originBytes[0], expected_originBytes[0], "Delegatecall should not change callee contract");
    Assert.equal(originBytes[1], expected_originBytes[1], "Delegatecall should not change callee contract");
    Assert.equal(originBytes[2], expected_originBytes[2], "Delegatecall should not change callee contract");
    Assert.equal(setContract.dynArrN(0), 0, "Delegatecall should not change callee contract");
    Assert.equal(setContract.dynArrN(1), 0, "Delegatecall should not change callee contract");
    Assert.equal(setContract.dynArrN(2), 0, "Delegatecall should not change callee contract");
  }


  function testDelegateArrayAndString() public {
    SetVariables setContract = SetVariables(DeployedAddresses.SetVariables());
    SetVariables_modified delegateContract = SetVariables_modified(DeployedAddresses.SetVariables_modified());
    bytes memory expectedBytes = "ABC";
    uint[] memory expectedArray = new uint[](3);
    expectedArray[0] = 111;
    expectedArray[1] = 222;
    expectedArray[2] = 333;
    string memory expectedString = "Expected";

    delegateContract.delegatecallsetBytesAndArray(DeployedAddresses.SetVariables(), expectedBytes, expectedArray);

    string memory resultString = delegateContract.stringN();
    string memory originString = setContract.stringN();

    Assert.equal(delegateContract.dynArrN(0), expectedArray[0], "Delegatecall on dynamic array as first argument");
    Assert.equal(delegateContract.dynArrN(1), expectedArray[1], "Delegatecall on dynamic array as first argument");
    Assert.equal(delegateContract.dynArrN(2), expectedArray[2], "Delegatecall on dynamic array as first argument");       
    Assert.equal(resultString, expectedString, "Delegatecall on string as second argument");

    Assert.equal(setContract.dynArrN(0), 0, "Delegatecall should not change callee contract");
    Assert.equal(setContract.dynArrN(1), 0, "Delegatecall should not change callee contract");
    Assert.equal(setContract.dynArrN(2), 0, "Delegatecall should not change callee contract");
    Assert.equal(originString, "Hello", "Delegatecall should not change callee contract"); 
  }
}
