import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/SetVariables.sol";
import "../contracts/SetVariables_modified.sol";

contract TestDelegateCallStatic {

  function testInitValues() public {
    SetVariables setContract = SetVariables(DeployedAddresses.SetVariables());
    Assert.equal(setContract.booleanN(), false, "Contract initial boolean value should be false");
    Assert.equal(setContract.intN(), 9999, "Contract initial intN value should be 9999");
    Assert.equal(setContract.uintN(), 8888, "Contract initial uintN value should be 8888");
    Assert.equal(setContract.addrN(), 0, "Contract initial addrN value should be 0x0");
    Assert.equal(setContract.threeBytesN(), bytes3(16777215), "Contract initial bytes3 value should be 16777215");
    Assert.equal(setContract.oneByteN(), byte(255), "Contract initial byte value should be 255");
    Assert.equal(uint(setContract.enumN()), 0, "Contract initial Alphabet value should be 'A'");
  }

  function testDelegateBool() public {
    SetVariables setContract = SetVariables(DeployedAddresses.SetVariables());
    SetVariables_modified delegateContract = SetVariables_modified(DeployedAddresses.SetVariables_modified());
    bool expected = true;
    delegateContract.delegatecallsetBoolN(DeployedAddresses.SetVariables(), expected);
    Assert.equal(delegateContract.booleanN(), expected, "Delegate call on booleanN should make it true");
    Assert.equal(setContract.booleanN(), false, "Delegate call should not alter the state of the called contract");
  }

  function testDelegateInt() public {
    SetVariables setContract = SetVariables(DeployedAddresses.SetVariables());
    SetVariables_modified delegateContract = SetVariables_modified(DeployedAddresses.SetVariables_modified());
    int expected = 101;
    delegateContract.delegatecallsetIntN(DeployedAddresses.SetVariables(), expected);
    Assert.equal(delegateContract.intN(), expected, "Delegate call on intN should be 101");
    Assert.equal(setContract.intN(), 9999, "Delegate call should not alter the state of the called contract");
  }

  function testDelegateUInt() public {
    SetVariables setContract = SetVariables(DeployedAddresses.SetVariables());
    SetVariables_modified delegateContract = SetVariables_modified(DeployedAddresses.SetVariables_modified());
    uint expected = 102;
    delegateContract.delegatecallsetUIntN(DeployedAddresses.SetVariables(), expected);
    Assert.equal(delegateContract.uintN(), expected, "Delegate call on uintN should be 102");
    Assert.equal(setContract.uintN(), 8888, "Delegate call should not alter the state of the called contract");
  }

  function testDelegateAddr() public {
    SetVariables setContract = SetVariables(DeployedAddresses.SetVariables());
    SetVariables_modified delegateContract = SetVariables_modified(DeployedAddresses.SetVariables_modified());
    address expected = DeployedAddresses.SetVariables();
    delegateContract.delegatecallsetAddrN(DeployedAddresses.SetVariables(), expected);
    Assert.equal(delegateContract.addrN(), expected, "Delegate call on address should be the callee address");
    Assert.equal(setContract.addrN(), 0, "Delegate call should not alter the state of the called contract");
  }

  function testDelegateThreeBytes() public {
    SetVariables setContract = SetVariables(DeployedAddresses.SetVariables());
    SetVariables_modified delegateContract = SetVariables_modified(DeployedAddresses.SetVariables_modified());
    bytes3 expected = bytes3(103);
    delegateContract.delegatecallsetThreeBytesN(DeployedAddresses.SetVariables(), expected);
    Assert.equal(delegateContract.threeBytesN(), expected, "Delegate call on bytes3 should be 103");
    Assert.equal(setContract.threeBytesN(), bytes3(16777215), "Delegate call should not alter the state of the called contract");
  }

  function testDelegateOneByte() public {
    SetVariables setContract = SetVariables(DeployedAddresses.SetVariables());
    SetVariables_modified delegateContract = SetVariables_modified(DeployedAddresses.SetVariables_modified());
    byte expected = byte(104);
    delegateContract.delegatecallsetByteN(DeployedAddresses.SetVariables(), expected);
    Assert.equal(delegateContract.oneByteN(), expected, "Delegate call on byte should be 104");
    Assert.equal(setContract.oneByteN(), byte(255), "Delegate call should not alter the state of the called contract");
  }

  function testDelegateEnum() public {
    SetVariables setContract = SetVariables(DeployedAddresses.SetVariables());
    SetVariables_modified delegateContract = SetVariables_modified(DeployedAddresses.SetVariables_modified());
    SetVariables_modified.Alphabet expected = SetVariables_modified.Alphabet.E;
    delegateContract.delegatecallsetNEnum(DeployedAddresses.SetVariables(), expected);
    Assert.equal(uint(delegateContract.enumN()), uint(expected), "Delegate call on enum should be 'E'");
    Assert.equal(uint(setContract.enumN()), 0, "Delegate call should not alter the state of the called contract");

//    uint[2] memory eq1 = [uint(1),2];
//    uint[2] memory eq2 = [uint(1),2];
//    Assert.equal(keccak256(eq1), keccak256(eq2), "Test");


  }

  function testDelegateArray() public {
    SetVariables setContract = SetVariables(DeployedAddresses.SetVariables());
    SetVariables_modified delegateContract = SetVariables_modified(DeployedAddresses.SetVariables_modified());
    uint[3] memory expected = [uint(111),222,333];
    delegateContract.delegatecallsetArrayN(DeployedAddresses.SetVariables(), expected);
    Assert.equal(delegateContract.arrN(0), expected[0], "Delegate call on array");
    Assert.equal(delegateContract.arrN(1), expected[1], "Delegate call on array");
    Assert.equal(delegateContract.arrN(2), expected[2], "Delegate call on array");
    Assert.equal(setContract.arrN(0), 0, "Delegate call on array");
    Assert.equal(setContract.arrN(1), 0, "Delegate call on array");
    Assert.equal(setContract.arrN(2), 0, "Delegate call on array");
  }


}
