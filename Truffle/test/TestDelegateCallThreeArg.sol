pragma solidity ^0.4.24;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/SetVariables.sol";
import "../contracts/SetVariables_modified.sol";

contract TestDelegateCallThreeArg {

  function testDelegateSetArrIntStr() public {
    SetVariables setContract = SetVariables(DeployedAddresses.SetVariables());
    SetVariables_modified delegateContract = SetVariables_modified(DeployedAddresses.SetVariables_modified());

    int expectedInt = 112233;
    uint[] memory expectedArray = new uint[](3);
    expectedArray[0] = 111;
    expectedArray[1] = 222;
    expectedArray[2] = 333;
    string memory expectedString = "Expected";

    delegateContract.delegatecallsetArrIntStr(DeployedAddresses.SetVariables(), expectedArray, expectedInt, expectedString);

    string memory resultString = delegateContract.stringN();
    string memory originString = setContract.stringN();

    int resultInt = delegateContract.intN();
    int originInt = setContract.intN();

    Assert.equal(delegateContract.dynArrN(0), expectedArray[0], "Delegatecall on dynamic array as first argument");
    Assert.equal(delegateContract.dynArrN(1), expectedArray[1], "Delegatecall on dynamic array as first argument");
    Assert.equal(delegateContract.dynArrN(2), expectedArray[2], "Delegatecall on dynamic array as first argument");  

    Assert.equal(resultString, expectedString, "Delegatecall on string as second argument");
    Assert.equal(resultInt, expectedInt, "Delegatecall on int as third argument");

    Assert.equal(setContract.dynArrN(0), 0, "Delegatecall should not change callee contract");
    Assert.equal(setContract.dynArrN(1), 0, "Delegatecall should not change callee contract");
    Assert.equal(setContract.dynArrN(2), 0, "Delegatecall should not change callee contract");
    Assert.equal(originString, "Hello", "Delegatecall should not change callee contract"); 
    Assert.equal(originInt, 9999, "Delegatecall should not change callee contract");
  }

  function testDelegateSetIntArrStr() public {
    SetVariables setContract = SetVariables(DeployedAddresses.SetVariables());
    SetVariables_modified delegateContract = SetVariables_modified(DeployedAddresses.SetVariables_modified());

    int expectedInt = 112233;
    uint[] memory expectedArray = new uint[](3);
    expectedArray[0] = 111;
    expectedArray[1] = 222;
    expectedArray[2] = 333;
    string memory expectedString = "Expected";

    delegateContract.delegatecallsetIntArrStr(DeployedAddresses.SetVariables(), expectedInt, expectedArray,  expectedString);

    string memory resultString = delegateContract.stringN();
    string memory originString = setContract.stringN();

    int resultInt = delegateContract.intN();
    int originInt = setContract.intN();

    Assert.equal(delegateContract.dynArrN(0), expectedArray[0], "Delegatecall on dynamic array as first argument");
    Assert.equal(delegateContract.dynArrN(1), expectedArray[1], "Delegatecall on dynamic array as first argument");
    Assert.equal(delegateContract.dynArrN(2), expectedArray[2], "Delegatecall on dynamic array as first argument");  

    Assert.equal(resultString, expectedString, "Delegatecall on string as second argument");
    Assert.equal(resultInt, expectedInt, "Delegatecall on int as third argument");

    Assert.equal(setContract.dynArrN(0), 0, "Delegatecall should not change callee contract");
    Assert.equal(setContract.dynArrN(1), 0, "Delegatecall should not change callee contract");
    Assert.equal(setContract.dynArrN(2), 0, "Delegatecall should not change callee contract");
    Assert.equal(originString, "Hello", "Delegatecall should not change callee contract"); 
    Assert.equal(originInt, 9999, "Delegatecall should not change callee contract");
  }

  function testDelegateSetArrStrInt() public {
    SetVariables setContract = SetVariables(DeployedAddresses.SetVariables());
    SetVariables_modified delegateContract = SetVariables_modified(DeployedAddresses.SetVariables_modified());

    int expectedInt = 112233;
    uint[] memory expectedArray = new uint[](3);
    expectedArray[0] = 111;
    expectedArray[1] = 222;
    expectedArray[2] = 333;
    string memory expectedString = "Expected";

    delegateContract.delegatecallsetArrStrInt(DeployedAddresses.SetVariables(), expectedArray, expectedString, expectedInt);

    string memory resultString = delegateContract.stringN();
    string memory originString = setContract.stringN();

    int resultInt = delegateContract.intN();
    int originInt = setContract.intN();

    Assert.equal(delegateContract.dynArrN(0), expectedArray[0], "Delegatecall on dynamic array as first argument");
    Assert.equal(delegateContract.dynArrN(1), expectedArray[1], "Delegatecall on dynamic array as first argument");
    Assert.equal(delegateContract.dynArrN(2), expectedArray[2], "Delegatecall on dynamic array as first argument");  

    Assert.equal(resultString, expectedString, "Delegatecall on string as second argument");
    Assert.equal(resultInt, expectedInt, "Delegatecall on int as third argument");

    Assert.equal(setContract.dynArrN(0), 0, "Delegatecall should not change callee contract");
    Assert.equal(setContract.dynArrN(1), 0, "Delegatecall should not change callee contract");
    Assert.equal(setContract.dynArrN(2), 0, "Delegatecall should not change callee contract");
    Assert.equal(originString, "Hello", "Delegatecall should not change callee contract"); 
    Assert.equal(originInt, 9999, "Delegatecall should not change callee contract");
  }
}
