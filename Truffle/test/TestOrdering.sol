pragma solidity ^0.4.24;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/SetVariables_Ordering.sol";

contract TestOrdering {

  function testInitValues() public {
    SetVariables_Ordering setContract = SetVariables_Ordering(DeployedAddresses.SetVariables_Ordering());
    Assert.equal(setContract.booleanN(), false, "Contract initial boolean value should be false");
    Assert.equal(setContract.intN(), 9999, "Contract initial intN value should be 9999");
    Assert.equal(setContract.uintN(), 8888, "Contract initial uintN value should be 8888");
    Assert.equal(setContract.addrN(), 0, "Contract initial addrN value should be 0x0");
    Assert.equal(setContract.threeBytesN(), bytes3(16777215), "Contract initial bytes3 value should be 16777215");
    Assert.equal(setContract.oneByteN(), byte(255), "Contract initial byte value should be 255");
    Assert.equal(uint(setContract.enumN()), 0, "Contract initial Alphabet value should be 'A'");
    Assert.equal(setContract.arrN(0), 0, "Contract initial array should be set to zero");
    Assert.equal(setContract.arrN(1), 0, "Contract initial array should be set to zero");
    Assert.equal(setContract.arrN(2), 0, "Contract initial array should be set to zero");
  }

  function testSetBoolN() public {
    SetVariables_Ordering setContract = SetVariables_Ordering(DeployedAddresses.SetVariables_Ordering());
    bool expected = true;
    setContract.setBoolN(expected);
    Assert.equal(setContract.booleanN(), expected, "Contract boolean should be true"); 
  }

  function testSetIntN() public {
    SetVariables_Ordering setContract = SetVariables_Ordering(DeployedAddresses.SetVariables_Ordering());
    int expected = 101;
    setContract.setIntN(expected);
    Assert.equal(setContract.intN(), expected, "Contract int should be 101");
  }

  function testSetUIntN() public {
    SetVariables_Ordering setContract = SetVariables_Ordering(DeployedAddresses.SetVariables_Ordering());
    uint expected = 102;
    setContract.setUIntN(expected);
    Assert.equal(setContract.uintN(), expected, "Contract uint should be 102");
  }

  function testSetAddrN() public {
    SetVariables_Ordering setContract = SetVariables_Ordering(DeployedAddresses.SetVariables_Ordering());
    address expected = tx.origin;
    setContract.setAddrN(expected);
    Assert.equal(setContract.addrN(), expected, "Contract address should be the origin");
  }

  function testSetThreeBytesN() public {
    SetVariables_Ordering setContract = SetVariables_Ordering(DeployedAddresses.SetVariables_Ordering());
    bytes3 expected = 103;
    setContract.setThreeBytesN(expected);
    Assert.equal(setContract.threeBytesN(), expected, "Contract bytes3 should be 103");
  }

  function testSetByteN() public {
    SetVariables_Ordering setContract = SetVariables_Ordering(DeployedAddresses.SetVariables_Ordering());
    byte expected = 104;
    setContract.setByteN(expected);
    Assert.equal(setContract.oneByteN(), expected, "Contract byte should be 104");
  }

  function testSetNEnum() public {
    SetVariables_Ordering setContract = SetVariables_Ordering(DeployedAddresses.SetVariables_Ordering());
    SetVariables_Ordering.Alphabet expected = SetVariables_Ordering.Alphabet.E;
    setContract.setNEnum(expected);
    Assert.equal(uint(setContract.enumN()), uint(expected), "Contract enum should be 'E'");
  }

  function testSetArray() public {
    SetVariables_Ordering setContract = SetVariables_Ordering(DeployedAddresses.SetVariables_Ordering());
    uint[3] memory expected = [uint(111),222,333];
    setContract.setArrayN(expected);
    Assert.equal(setContract.arrN(0), 111, "Contract array should be [111,222,333]");
    Assert.equal(setContract.arrN(1), 222, "Contract array should be [111,222,333]");
    Assert.equal(setContract.arrN(2), 333, "Contract array should be [111,222,333]");
  }
}
