const Uncheck = artifacts.require('./Uncheck.sol')
contract('Uncheck', function ([owner, donor]) {

    let catchRevert = require("./exception.js").catchRevert;
    let uncheck;

    beforeEach('Contract Setup', async function () {
        uncheck = await Uncheck.new()
    });

    it('uncheckSend should not revert Test', async function() {
      await uncheck.uncheckSend();
    });

    it('uncheckCall should not revert Test', async function() {
      await uncheck.uncheckCall(uncheck.address);
    });

    it('uncheckCallcode should not revert Test', async function() {
      await uncheck.uncheckCallcode(uncheck.address);
    });

    it('uncheckDelegatecall should not revert Test', async function() {
      await uncheck.uncheckDelegatecall(uncheck.address);
    });

})
