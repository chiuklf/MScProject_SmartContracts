const ReEntrancy_modified = artifacts.require('./ReEntrancy_modified.sol')
contract('ReEntrancy_modified', function ([owner, donor]) {

    let catchRevert = require("./exception.js").catchRevert;
    let reEntrancy;

    beforeEach('setup contract for each test', async function () {
        reEntrancy = await ReEntrancy_modified.new()
    });

    it('Payable', async function () {
      await reEntrancy.sendTransaction({ value: 10000, from: donor });

      const reEntrancyAddress = await reEntrancy.address;
      assert.equal(web3.eth.getBalance(reEntrancyAddress).toNumber(), 10000);
    });

    it('claim should pass', async function() {
      await reEntrancy.sendTransaction({ value: 10000, from: donor });
      await reEntrancy.claim()
      let claimed = await reEntrancy.claimed();
      assert.equal(claimed, true);
    });

    it('claim should revert', async function() {
      await catchRevert(reEntrancy.claim());
      let claimed = await reEntrancy.claimed();
      assert.equal(claimed, false);
    });

    it('claimSetN should pass', async function() {
      await reEntrancy.sendTransaction({ value: 10000, from: donor });
      await reEntrancy.claimSetN()
      let claimed = await reEntrancy.claimed();
      let intN = await reEntrancy.intN();
      assert.equal(claimed, true);
      assert.equal(intN, 99);
    });

    it('claimSetN should revert', async function() {
      await catchRevert(reEntrancy.claimSetN());
      let claimed = await reEntrancy.claimed();
      let intN = await reEntrancy.intN();
      assert.equal(claimed, false);
      assert.equal(intN, 0);
    });
})
