const Uncheck_modified = artifacts.require('./Uncheck_modified.sol')
contract('Uncheck_modified', function ([owner, donor]) {

    let catchRevert = require("./exception.js").catchRevert;
    let uncheck;

    beforeEach('Contract Setup', async function () {
        uncheck = await Uncheck_modified.new()
    });

    it('uncheckSend should not revert Test', async function() {
      await uncheck.sendTransaction({ value: 120, from: donor });
      await uncheck.uncheckSend();
    });

    it('uncheckSend should revert Test', async function() {
      await catchRevert(uncheck.uncheckSend());
    });

    it('uncheckCall should revert Test', async function() {
      await catchRevert(uncheck.uncheckCall(uncheck.address));
    });

    it('uncheckCallcode should revert Test', async function() {
      await catchRevert(uncheck.uncheckCallcode(uncheck.address));
    });

    it('uncheckDelegatecall should revert Test', async function() {
      await catchRevert(uncheck.uncheckDelegatecall(uncheck.address));
    });

})
