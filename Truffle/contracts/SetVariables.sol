pragma solidity ^0.4.24;

contract SetVariables {
  bool public booleanN;
  int public intN;
  uint public uintN;
  //fixed fixN; Fixed Point Numbers not fully supported by solidity
  address public addrN;
  bytes3 public threeBytesN;
  byte public oneByteN;

  enum Alphabet {A,B,C,D,E,F,G,H,I,J,K,L}
  Alphabet public enumN;

  uint[3] public arrN;

//Dynamic Types
  bytes public bytesN;
  string public stringN;
  uint[] public dynArrN;
  uint[][] public doubleArrN;

  function SetVariables() public {
      booleanN = false;
      intN = 9999;
      uintN = 8888;
      addrN = 0x0;
      threeBytesN = bytes3(16777215);
      oneByteN = byte(255);
      enumN = Alphabet.A;
      arrN = [uint(0),0,0];

      bytesN = "XYZ";
      stringN = "Hello";
      dynArrN = [0,0,0];
      doubleArrN = [[0,0],[0,0],[0,0]];
  }

  function setBoolN(bool _bool) public {
    booleanN = _bool;
  }

  function setIntN(int _n) public {
    intN = _n;
  }
  
  function setUIntN(uint _n) public {
    uintN = _n;
  }
  
  function setAddrN(address _addr) public {
    addrN = _addr;
  }
  
  function setThreeBytesN(bytes3 _n) public {
    threeBytesN = _n;
  }
  
  function setByteN(byte _n) public {
    oneByteN = _n;
  }
  
  function setNEnum(Alphabet _enum) public {
    enumN = _enum;
  }  
  
  function setArrayN(uint[3] _arr) public {
    arrN = _arr;
  }

  function setBytes(bytes _bytes) public {
    bytesN = _bytes;
  }
  
  function setString(string _string) public {
    stringN = _string;
  }  
  
  function setDynArray(uint[] _arr) public {
    dynArrN = _arr;
  }

  function setStringAndBytes(string _string, bytes _bytes) public {
    stringN = _string;
    bytesN = _bytes;
  }

  function setBytesAndArray(bytes _bytes, uint[] _arr) public {
    bytesN = _bytes;
    dynArrN = _arr;
  }

  function setArrayAndString(uint[] _arr, string _string) public {
    dynArrN = _arr;
    stringN = _string;
  }

  function setArrIntStr(uint[] _arr, int _n, string _string) public {
  	dynArrN = _arr;
  	intN = _n;
  	stringN = _string;
  }
    
  function setIntArrStr(int _n, uint[] _arr, string _string) public {
  	dynArrN = _arr;
  	intN = _n;
  	stringN = _string;
  }

  function setArrStrInt(uint[] _arr, string _string, int _n) public {
  	dynArrN = _arr;
  	intN = _n;
  	stringN = _string;
  }
}
