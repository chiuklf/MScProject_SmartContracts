pragma solidity ^0.4.24;

contract SetVariables_modified {
  bool public booleanN;
  int public intN;
  uint public uintN;
  //fixed fixN; Fixed Point Numbers not fully supported by solidity
  address public addrN;
  bytes3 public threeBytesN;
  byte public oneByteN;

  enum Alphabet {A,B,C,D,E,F,G,H,I,J,K,L}
  Alphabet public enumN;

  uint[3] public arrN;

//Dynamic Types
  bytes public bytesN;
  string public stringN;
  uint[] public dynArrN;
  uint[][] public doubleArrN;

  function delegatecallSetVariables(address _contractAddr) public {
		require(_contractAddr.delegatecall(bytes4(keccak256("SetVariables()"))));
	}

  function delegatecallsetBoolN(address _contractAddr, bool _bool) public {
		require(_contractAddr.delegatecall(bytes4(keccak256("setBoolN(bool)")),_bool));
	}

  function delegatecallsetIntN(address _contractAddr, int _n) public {
		require(_contractAddr.delegatecall(bytes4(keccak256("setIntN(int256)")),_n));
	}
  
  function delegatecallsetUIntN(address _contractAddr, uint _n) public {
		require(_contractAddr.delegatecall(bytes4(keccak256("setUIntN(uint256)")),_n));
	}
  
  function delegatecallsetAddrN(address _contractAddr, address _addr) public {
		require(_contractAddr.delegatecall(bytes4(keccak256("setAddrN(address)")),_addr));
	}
  
  function delegatecallsetThreeBytesN(address _contractAddr, bytes3 _n) public {
		require(_contractAddr.delegatecall(bytes4(keccak256("setThreeBytesN(bytes3)")),_n));
	}
  
  function delegatecallsetByteN(address _contractAddr, byte _n) public {
		require(_contractAddr.delegatecall(bytes4(keccak256("setByteN(bytes1)")),_n));
	}
  
  function delegatecallsetNEnum(address _contractAddr, Alphabet _enum) public {
		require(_contractAddr.delegatecall(bytes4(keccak256("setNEnum(uint8)")),_enum));
	}  
  
  function delegatecallsetArrayN(address _contractAddr, uint[3] _arr) public {
		require(_contractAddr.delegatecall(bytes4(keccak256("setArrayN(uint256[3])")),_arr));
	}

  function delegatecallsetBytes(address _contractAddr, bytes _bytes) public {
		require(_contractAddr.delegatecall(bytes4(keccak256("setBytes(bytes)")),0x20,_bytes.length,_bytes));
	}
  
  function delegatecallsetString(address _contractAddr, string _string) public {
		require(_contractAddr.delegatecall(bytes4(keccak256("setString(string)")),0x20,bytes(_string).length,_string));
	}  
  
  function delegatecallsetDynArray(address _contractAddr, uint[] _arr) public {
		require(_contractAddr.delegatecall(bytes4(keccak256("setDynArray(uint256[])")),0x20,_arr.length,_arr));
	}

  function delegatecallsetStringAndBytes(address _contractAddr, string _string, bytes _bytes) public {
		require(_contractAddr.delegatecall(bytes4(keccak256("setStringAndBytes(string,bytes)")),0x40,0x80,bytes(_string).length,_string,_bytes.length,_bytes));
	}

  function delegatecallsetBytesAndArray(address _contractAddr, bytes _bytes, uint[] _arr) public {
		require(_contractAddr.delegatecall(bytes4(keccak256("setBytesAndArray(bytes,uint256[])")),0x40,0x80,_bytes.length,_bytes,_arr.length,_arr));
	}

  function delegatecallsetArrayAndString(address _contractAddr, uint[] _arr, string _string) public {
		require(_contractAddr.delegatecall(bytes4(keccak256("setArrayAndString(uint256[],string)")),0x40,0x40+(uint256(0x20)*(_arr.length + 1)),_arr.length,_arr,bytes(_string).length,_string));
	}

  function delegatecallsetArrIntStr(address _contractAddr, uint[] _arr, int _n, string _string) public {
		require(_contractAddr.delegatecall(bytes4(keccak256("setArrIntStr(uint256[],int256,string)")),0x60,_n,0x60+(uint256(0x20)*(_arr.length + 1)),_arr.length,_arr,bytes(_string).length,_string));
	}
    
  function delegatecallsetIntArrStr(address _contractAddr, int _n, uint[] _arr, string _string) public {
		require(_contractAddr.delegatecall(bytes4(keccak256("setIntArrStr(int256,uint256[],string)")),_n,0x60,0x60+(uint256(0x20)*(_arr.length + 1)),_arr.length,_arr,bytes(_string).length,_string));
	}

  function delegatecallsetArrStrInt(address _contractAddr, uint[] _arr, string _string, int _n) public {
		require(_contractAddr.delegatecall(bytes4(keccak256("setArrStrInt(uint256[],string,int256)")),0x60,0x60+(uint256(0x20)*(_arr.length + 1)),_n,_arr.length,_arr,bytes(_string).length,_string));
	}
}
