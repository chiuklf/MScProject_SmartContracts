pragma solidity ^0.4.24;

contract Uncheck {

    function uncheckSend() public {
      msg.sender.send(111);
    }

    function uncheckCall(address _addr) public {
      _addr.call(msg.data);
    }

    function uncheckCallcode(address _addr) public {
      _addr.callcode(msg.data);
    }

    function uncheckDelegatecall(address _addr) public {
      _addr.delegatecall(msg.data);
    }

    function() public payable {
      if (msg.value == 0) revert();
    }
}
