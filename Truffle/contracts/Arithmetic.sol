pragma solidity ^0.4.24;

contract Arithmetic {
    
    uint public a;
    uint public b;
    uint public c;
    uint public d;

    function Arithmetic() {
    	a = 1;
    	b = 2;
    	c = 3;
    	d = 4;
    }

    function noBrackets() public {
        a = b + c * d;
    }

    function withBrackets() public {
    	a = (b+c)*(c+d);
    }

    function inConditions() public {
    	if (2+b*c == b*d) {
    		a = 99;
    	}
    }

    function underflow() public {
    	a -= b + c * d;
    }

    function overflow() public {
    	uint limit = 115792089237316195423570985008687907853269984665640564039457584007913129639935;
    	a = limit + 1;
    }

}