pragma solidity ^0.4.24;

contract SetVariables_Ordering_Original {
  address public addrN;
  bool public booleanN;

  uint public uintN;
  int public intN;
  string public stringN;
  bytes3 public threeBytesN;
  byte public oneByteN;

  uint public extraInt;

  enum Alphabet {A,B,C,D,E,F,G,H,I,J,K,L}
  Alphabet public enumN;

  string public extraString;

  uint[3] public arrN;

  bytes public bytesN;

  function SetVariables() public {
      booleanN = false;
      intN = 9999;
      uintN = 8888;
      addrN = 0x0;
      threeBytesN = bytes3(16777215);
      oneByteN = byte(255);
      enumN = Alphabet.A;
      arrN = [uint(0),0,0];

      bytesN = "XYZ";
  }

  function setBoolN(bool _bool) public {
    booleanN = _bool;
  }

  function setIntN(int _n) public {
    intN = _n;
  }
  
  function setUIntN(uint _n) public {
    uintN = _n;
  }
  
  function setAddrN(address _addr) public {
    addrN = _addr;
  }
  
  function setThreeBytesN(bytes3 _n) public {
    threeBytesN = _n;
  }
  
  function setByteN(byte _n) public {
    oneByteN = _n;
  }
  
  function setNEnum(Alphabet _enum) public {
    enumN = _enum;
  }  
  
  function setArrayN(uint[3] _arr) public {
    arrN = _arr;
  }

  function setBytes(bytes _bytes) public {
    bytesN = _bytes;
  }
  


}
