pragma solidity ^0.4.24;

contract ReEntrancy_modified {

    bool public claimed;
    int public intN;

    function claim() public {
        require(!claimed);
        
        claimed = true;
    	require(msg.sender.send(100));
	}

    function claimSetN() public {
        require(!claimed);
        claimed = true;
        
        intN = 99;
    	require(msg.sender.send(100));
	}

    function() payable public {}

}
