pragma solidity ^0.4.24;

contract Uncheck_modified {

    function uncheckSend() public {
      
    	require(msg.sender.send(111));
	}

    function uncheckCall(address _addr) public {
      
    	require(_addr.call(msg.data));
	}

    function uncheckCallcode(address _addr) public {
      require(_addr.callcode(msg.data));
    }

    function uncheckDelegatecall(address _addr) public {
      require(_addr.delegatecall(msg.data));
    }

    function() public payable {
      if (msg.value == 0) revert();
    }
}
