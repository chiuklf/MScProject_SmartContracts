pragma solidity ^0.4.24;

contract ReEntrancy {

    bool public claimed;
    int public intN;

    function claim() public {
        require(!claimed);
        require(msg.sender.send(100));
        claimed = true;
    }

    function claimSetN() public {
        require(!claimed);
        claimed = true;
        require(msg.sender.send(100));
        intN = 99;
    }

    function() payable public {}

}
