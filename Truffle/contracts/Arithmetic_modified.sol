pragma solidity ^0.4.24;

library SafeMath {

  function mul(uint256 _a, uint256 _b) internal pure returns (uint256) {
    // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
    // benefit is lost if 'b' is also tested.
    // See: https://github.com/OpenZeppelin/openzeppelin-solidity/pull/522
    if (_a == 0) {
      return 0;
    }

    uint256 c = _a * _b;
    require(c / _a == _b);

    return c;
  }

  function div(uint256 _a, uint256 _b) internal pure returns (uint256) {
    require(_b > 0); // Solidity only automatically asserts when dividing by 0
    uint256 c = _a / _b;
    // assert(_a == _b * c + _a % _b); // There is no case in which this doesn't hold

    return c;
  }

  /**
  * @dev Subtracts two numbers, reverts on overflow (i.e. if subtrahend is greater than minuend).
  */
  function sub(uint256 _a, uint256 _b) internal pure returns (uint256) {
    require(_b <= _a);
    uint256 c = _a - _b;

    return c;
  }

  /**
  * @dev Adds two numbers, reverts on overflow.
  */
  function add(uint256 _a, uint256 _b) internal pure returns (uint256) {
    uint256 c = _a + _b;
    require(c >= _a);

    return c;
  }

  /**
  * @dev Divides two numbers and returns the remainder (unsigned integer modulo),
  * reverts when dividing by zero.
  */
  function mod(uint256 a, uint256 b) internal pure returns (uint256) {
    require(b != 0);
    return a % b;
  }
}

contract Arithmetic_modified {

    using SafeMath for uint;    
    uint public a;
    uint public b;
    uint public c;
    uint public d;

    function Arithmetic_modified() {
    	a = 1;
    	b = 2;
    	c = 3;
    	d = 4;
    }

    function noBrackets() public {
        a=b.add((c.mul(d)));
    }

    function withBrackets() public {
    	a=((b.add(c)).mul((c.add(d))));
    }

    function inConditions() public {
    	if (uint(2).add((b.mul(c))) == (b.mul(d))) {
    		a = 99;
    	}
    }

    function underflow() public {
    	a=a.sub(b).add((c.mul(d)));
    }

    function overflow() public {
    	uint limit = 115792089237316195423570985008687907853269984665640564039457584007913129639935;
    	a=limit.add(1);
    }

}
